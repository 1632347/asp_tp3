﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Session
{
    public static class SessionExtensions
    {
        private const string CLIENT_ID_KEY = "CLIENT_ID";

        public static Guid? GetIdClient(this ISession session)
        {
            var clientId = session.GetString(CLIENT_ID_KEY);

            if (clientId == null)
            {
                return null;
            }
            else
            {
                return Guid.Parse(clientId);
            }
        }

        public static void SetIdClient(this ISession session, Guid id)
        {
            session.SetString(CLIENT_ID_KEY, id.ToString());
        }
    }

}
