﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class ItemMenu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "La catégorie est requise.")]
        [MaxLength(50)]
        public string Catégorie { get; set; }

        [Required(ErrorMessage = "Le nom est requis.")][MaxLength(150)]
        public string Nom { get; set; }

        [MaxLength(400)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Le prix est requis")]
        [Range(0.01, 200, ErrorMessage = "Le prix doit être entre 0.01 $ et 200 $.")]
        public double Prix { get; set; }

        public string PhotoMimeType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public byte[] Photo { get; set; }

        [Required]
        public bool? Actif { get; set; }

        [NotMapped]
        public bool EstActif { get => Actif ?? true; }
    }
}
