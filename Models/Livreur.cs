﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class Livreur
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200)]
        [Required(ErrorMessage = "Le nom est requis.")]
        public string Nom { get; set; }

        [StringLength(30)]
        [Phone(ErrorMessage = "Le numéro de téléphone n'est pas valide.")]
        [Display(Name = "Numéro de téléphone")]
        public string Téléphone { get; set; }

        public bool Désactivé { get; set; }

        public ICollection<Commande> Commandes { get; set; }
    }
}
