﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class Panier : IPanier
    {
        public Panier()
        {
            Items = new List<ItemPanier>();
        }

        public virtual ICollection<ItemPanier> Items { get; set; }

        public double Total => Items.Sum(item => item.Item.Prix * item.Quantité);


        public virtual void Vider()
        {
            Items.Clear();
        }

        public void AjouterItem(ItemMenu item, int quantité)
        {
            Contract.Assert(item != null);

            var itemExistant = Items.FirstOrDefault(_ => _.Item.Id == item.Id);
            if (itemExistant == null)
            {
                itemExistant = new ItemPanier() { Item = item, Quantité = 0 };
                Items.Add(itemExistant);
            }

            AjusterQuantité(item.Id, itemExistant.Quantité + quantité);
        }

        public virtual void AjusterQuantité(Guid id, int quantité)
        {
            var item = Items.FirstOrDefault(_ => _.Item.Id == id);
            if (item != null)
            {
                item.Quantité = Math.Max(0, quantité);
                if (item.Quantité == 0)
                {
                    Items.Remove(item);
                }
            }
        }

    }
}
