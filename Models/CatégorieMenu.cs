﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class CatégorieMenu
    {
        public CatégorieMenu(string nom, int ordre)
        {
            Nom = nom;
            Ordre = ordre;
        }

        [Key]
        [Required]
        [MaxLength(50)]
        public virtual string Nom { get; set; }

        [Required]
        public virtual int Ordre { get; set; }

        [ForeignKey(nameof(ItemMenu.Catégorie))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Pending>")]
        public virtual ICollection<ItemMenu> Items { get; set; }

        [NotMapped]
        public IEnumerable<ItemMenu> ItemsActifs { get => Items.Where(_ => _.Actif == true);  }
    }
}
