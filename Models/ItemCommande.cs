﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace GestionRestaurant.Models
{
    public class ItemCommande : ItemPanier
    {

        public ItemCommande()
        {

        }

        public ItemCommande (ItemPanier item)
        {
            Contract.Assert(item != null);
            Item = item.Item;
            Quantité = item.Quantité;
            PrixUnitaire = item.Item.Prix;            
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Display(Name ="Prix Unitaire")]
        public double PrixUnitaire { get; set; }

        [NotMapped]
        public double PrixTotal { get => PrixUnitaire * Quantité; }
    }
}
