﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GestionRestaurant.Models;
using GestionRestaurant.Session;

namespace GestionRestaurant.Pages.Clients
{
    public class CreerModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public CreerModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Client Client { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Clients.Add(Client);
            await _context.SaveChangesAsync();

            HttpContext.Session.SetIdClient(Client.Id);

            return RedirectToPage("/Paniers/Commander");
        }
    }
}