﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GestionRestaurant.Session;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GestionRestaurant.Pages.Paniers

{
    public class IndexModel : PageModel
    {
        private Models.ContexteRestaurant Restaurant { get; set; }
        public Models.IPanier Panier { get; set; }

        public IndexModel(Models.ContexteRestaurant restaurant, Models.IPanier panier)
        {
            Restaurant = restaurant;
            Panier = panier;
        }

        [BindProperty]
        public Dictionary<string, int> Items { get; set; }


        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            AjusterPanier();
            return Page();
        }

        public async Task<IActionResult> OnPostSupprimer(Guid id)
        {
            var item = await Restaurant.Menu.FindAsync(id);
            if (item == null)
            {
                return BadRequest();
            }

            AjusterPanier(); // Ajuste le panier avec le contenu du post, puis supprime l'élément demandé
            Panier.AjusterQuantité(item.Id, 0);
            return Page();
        }

        public IActionResult OnPostCommander()
        {
            AjusterPanier();

            if (HttpContext.Session.GetIdClient() == null)
            {
                return Redirect("~/Clients/Creer");
            }
            else
            {
                return Redirect("~/Paniers/Commander");
            }
        }

        protected void AjusterPanier()
        {
            foreach (var item in Items)
            {
                if (Guid.TryParse(item.Key, out var guid))
                {
                    Panier.AjusterQuantité(guid, item.Value);
                }
            }
        }
    }
}