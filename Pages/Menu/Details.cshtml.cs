﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Menu
{
    public class DetailsModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public DetailsModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public ItemMenu ItemMenu { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ItemMenu = await _context.Menu.FirstOrDefaultAsync(m => m.Id == id);

            if (ItemMenu == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
