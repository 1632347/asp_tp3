﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GestionRestaurant.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace GestionRestaurant.Pages.Menu
{
    public class CreateModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public CreateModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await LoadData();
            return Page();
        }

        public IEnumerable<SelectListItem> Catégories { get; private set; }


        [BindProperty]
        public ItemMenu Item { get; set; }

        [BindProperty]
        public IFormFile Photo { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Photo != null && !Photo.ContentType.StartsWith("image/", StringComparison.InvariantCulture))
            {
                ModelState.AddModelError(nameof(Photo), "La photo doit être une image valide.");
            }

            if (!ModelState.IsValid)
            {
                await LoadData();
                return Page();
            }
                
            if (Photo!= null)
            {
                using (var stream = Photo.OpenReadStream())
                {
                    Item.PhotoMimeType = Photo.ContentType;
                    Item.Photo = new byte[Photo.Length];
                    await stream.ReadAsync(Item.Photo, 0, Item.Photo.Length);
                }
            }

            _context.Menu.Add(Item);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

        private async Task LoadData()
        {
            Catégories = await _context.Catégories.Select(_ => new SelectListItem(_.Nom, _.Nom)).ToListAsync();
        }
    }


}