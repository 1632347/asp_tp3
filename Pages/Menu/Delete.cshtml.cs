﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Menu
{
    public class DeleteModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public DeleteModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        [BindProperty]
        public ItemMenu Item { get; set; }

        public bool DéjàCommandé { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Item = await _context.Menu.FirstOrDefaultAsync(m => m.Id == id);

            if (Item == null)
            {
                return NotFound();
            }

            DéjàCommandé = await _context.Commandes.AnyAsync(_ => _.Items.Any(item => item.Item.Id == id));
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Item = await _context.Menu.FindAsync(id);

            if (Item != null)
            {
                DéjàCommandé = await _context.Commandes.AnyAsync(_ => _.Items.Any(item => item.Item.Id == id));
                if (DéjàCommandé)
                {
                    Item.Actif = false;
                }
                else
                {
                    _context.Menu.Remove(Item);
                }
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
