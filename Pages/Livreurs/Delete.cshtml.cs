﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Livreurs
{
    public class DeleteModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public DeleteModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        [BindProperty]
        public Livreur Livreur { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Livreur = await _context.Livreurs
                .Include(_ => _.Commandes)
                .SingleOrDefaultAsync(_ => _.Id == id);

            if (Livreur == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Livreur = await _context.Livreurs
                .Include(_ => _.Commandes)
                .SingleOrDefaultAsync(_ => _.Id == id);

            if (Livreur != null)
            {
                if (Livreur.Commandes.Any())
                {
                    Livreur.Désactivé = true;
                }
                else
                {
                    _context.Livreurs.Remove(Livreur);
                }

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
