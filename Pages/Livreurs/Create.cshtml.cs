﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GestionRestaurant.Models;

namespace GestionRestaurant.Pages.Livreurs
{
    public class CreateModel : PageModel
    {
        private readonly GestionRestaurant.Models.ContexteRestaurant _context;

        public CreateModel(GestionRestaurant.Models.ContexteRestaurant context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Livreur Livreur { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Livreurs.Add(Livreur);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}