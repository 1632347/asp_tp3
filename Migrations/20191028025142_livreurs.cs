﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GestionRestaurant.Migrations
{
    public partial class ajout_livreurs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Désactivé",
                table: "Livreur",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Nom",
                table: "Livreur",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Téléphone",
                table: "Livreur",
                maxLength: 30,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Désactivé",
                table: "Livreur");

            migrationBuilder.DropColumn(
                name: "Nom",
                table: "Livreur");

            migrationBuilder.DropColumn(
                name: "Téléphone",
                table: "Livreur");
        }
    }
}
