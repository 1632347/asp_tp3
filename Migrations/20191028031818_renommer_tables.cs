﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GestionRestaurant.Migrations
{
    public partial class renommer_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commande_Client_ClientId",
                table: "Commande");

            migrationBuilder.DropForeignKey(
                name: "FK_Commande_Livreur_LivreurId",
                table: "Commande");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemCommande_Commande_CommandeId",
                table: "ItemCommande");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Livreur",
                table: "Livreur");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Commande",
                table: "Commande");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Client",
                table: "Client");

            migrationBuilder.RenameTable(
                name: "Livreur",
                newName: "Livreurs");

            migrationBuilder.RenameTable(
                name: "Commande",
                newName: "Commandes");

            migrationBuilder.RenameTable(
                name: "Client",
                newName: "Clients");

            migrationBuilder.RenameIndex(
                name: "IX_Commande_LivreurId",
                table: "Commandes",
                newName: "IX_Commandes_LivreurId");

            migrationBuilder.RenameIndex(
                name: "IX_Commande_ClientId",
                table: "Commandes",
                newName: "IX_Commandes_ClientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Livreurs",
                table: "Livreurs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Commandes",
                table: "Commandes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Clients",
                table: "Clients",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Commandes_Clients_ClientId",
                table: "Commandes",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Commandes_Livreurs_LivreurId",
                table: "Commandes",
                column: "LivreurId",
                principalTable: "Livreurs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemCommande_Commandes_CommandeId",
                table: "ItemCommande",
                column: "CommandeId",
                principalTable: "Commandes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Commandes_Clients_ClientId",
                table: "Commandes");

            migrationBuilder.DropForeignKey(
                name: "FK_Commandes_Livreurs_LivreurId",
                table: "Commandes");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemCommande_Commandes_CommandeId",
                table: "ItemCommande");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Livreurs",
                table: "Livreurs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Commandes",
                table: "Commandes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Clients",
                table: "Clients");

            migrationBuilder.RenameTable(
                name: "Livreurs",
                newName: "Livreur");

            migrationBuilder.RenameTable(
                name: "Commandes",
                newName: "Commande");

            migrationBuilder.RenameTable(
                name: "Clients",
                newName: "Client");

            migrationBuilder.RenameIndex(
                name: "IX_Commandes_LivreurId",
                table: "Commande",
                newName: "IX_Commande_LivreurId");

            migrationBuilder.RenameIndex(
                name: "IX_Commandes_ClientId",
                table: "Commande",
                newName: "IX_Commande_ClientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Livreur",
                table: "Livreur",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Commande",
                table: "Commande",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Client",
                table: "Client",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Commande_Client_ClientId",
                table: "Commande",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Commande_Livreur_LivreurId",
                table: "Commande",
                column: "LivreurId",
                principalTable: "Livreur",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemCommande_Commande_CommandeId",
                table: "ItemCommande",
                column: "CommandeId",
                principalTable: "Commande",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
