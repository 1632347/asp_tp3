﻿// <auto-generated />
using System;
using GestionRestaurant.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace GestionRestaurant.Migrations
{
    [DbContext(typeof(ContexteRestaurant))]
    [Migration("20191028025142_livreurs")]
    partial class ajout_livreurs
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GestionRestaurant.Models.CatégorieMenu", b =>
                {
                    b.Property<string>("Nom")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50);

                    b.Property<int>("Ordre");

                    b.HasKey("Nom");

                    b.ToTable("Catégories");

                    b.HasData(
                        new
                        {
                            Nom = "Pizzas",
                            Ordre = 100
                        },
                        new
                        {
                            Nom = "Breuvages",
                            Ordre = 200
                        },
                        new
                        {
                            Nom = "Desserts",
                            Ordre = 300
                        },
                        new
                        {
                            Nom = "À Côtés",
                            Ordre = 400
                        });
                });

            modelBuilder.Entity("GestionRestaurant.Models.Client", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adresse")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Téléphone")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.HasKey("Id");

                    b.ToTable("Client");
                });

            modelBuilder.Entity("GestionRestaurant.Models.Commande", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ClientId");

                    b.Property<Guid?>("LivreurId");

                    b.Property<DateTime>("MomentCommandé");

                    b.Property<DateTime>("MomentLivraisonEstimé");

                    b.Property<DateTime?>("MomentLivré");

                    b.Property<int>("État");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("LivreurId");

                    b.ToTable("Commande");
                });

            modelBuilder.Entity("GestionRestaurant.Models.ItemCommande", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("CommandeId");

                    b.Property<Guid>("ItemId");

                    b.Property<double>("PrixUnitaire");

                    b.Property<int>("Quantité");

                    b.HasKey("Id");

                    b.HasIndex("CommandeId");

                    b.HasIndex("ItemId");

                    b.ToTable("ItemCommande");
                });

            modelBuilder.Entity("GestionRestaurant.Models.ItemMenu", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Actif")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("Catégorie")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Description")
                        .HasMaxLength(400);

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<byte[]>("Photo");

                    b.Property<string>("PhotoMimeType");

                    b.Property<double>("Prix");

                    b.HasKey("Id");

                    b.HasIndex("Catégorie");

                    b.ToTable("Menu");

                    b.HasData(
                        new
                        {
                            Id = new Guid("185b5f1d-80ec-458b-95cb-e00e901b3406"),
                            Actif = true,
                            Catégorie = "Pizzas",
                            Description = "Portion double de pepperoni et fromage mozzarella à profusion",
                            Nom = "Pizza au pépéronni",
                            Prix = 15.99
                        },
                        new
                        {
                            Id = new Guid("a252df95-317e-4d82-8367-c95138934b37"),
                            Actif = true,
                            Catégorie = "Pizzas",
                            Description = "Champignons, poivrons verts, oignons rouges, tomates mûres et fromage mozzarella.",
                            Nom = "Pizza aux légumes",
                            Prix = 16.989999999999998
                        },
                        new
                        {
                            Id = new Guid("72d54fd4-5cf4-461b-86d7-3fb8f23dfda5"),
                            Actif = true,
                            Catégorie = "Pizzas",
                            Description = "Jambon, ananas et généreuse portion de fromage mozzarella.",
                            Nom = "Pizza hawaïenne",
                            Prix = 19.989999999999998
                        },
                        new
                        {
                            Id = new Guid("6c701dcf-6960-45d7-9a57-b54202a35b1d"),
                            Actif = true,
                            Catégorie = "Pizzas",
                            Description = "Pepperoni, champignons, poivrons verts et fromage mozzarella.",
                            Nom = "Pizza toute garnie",
                            Prix = 17.489999999999998
                        },
                        new
                        {
                            Id = new Guid("9e027e34-9da7-4683-8362-38104603a6d8"),
                            Actif = true,
                            Catégorie = "Desserts",
                            Description = "Fraîchement cuit. Servi chaud et moelleux. Essayez-le aujourd’hui!",
                            Nom = "Biscuit",
                            Prix = 4.9900000000000002
                        },
                        new
                        {
                            Id = new Guid("762d1473-6a87-4339-85c1-aced70dd82d0"),
                            Actif = true,
                            Catégorie = "Desserts",
                            Description = "9 carrés moelleux sortis du four.",
                            Nom = "Brownies",
                            Prix = 9.4900000000000002
                        },
                        new
                        {
                            Id = new Guid("a1346ec8-fd6c-47f1-942c-03cc4b2bcdab"),
                            Actif = true,
                            Catégorie = "Breuvages",
                            Description = "Gâtez vos papilles avec le goût de lime et de citron frais de 7UP®. Sans caféine, mais des saveurs naturelles à 100 %. 7UP®, le goût de l’effervescence.",
                            Nom = "7-up (2L)",
                            Prix = 3.4900000000000002
                        },
                        new
                        {
                            Id = new Guid("fb7f1dc9-862c-4eba-96d9-1f99dbe2c6d1"),
                            Actif = true,
                            Catégorie = "Breuvages",
                            Description = "La boisson gazeuse audacieuse, vivifiante et délicieusement pétillante.",
                            Nom = "Pepsi (2L)",
                            Prix = 3.4900000000000002
                        });
                });

            modelBuilder.Entity("GestionRestaurant.Models.Livreur", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Désactivé");

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Téléphone")
                        .HasMaxLength(30);

                    b.HasKey("Id");

                    b.ToTable("Livreur");
                });

            modelBuilder.Entity("GestionRestaurant.Models.Commande", b =>
                {
                    b.HasOne("GestionRestaurant.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GestionRestaurant.Models.Livreur", "Livreur")
                        .WithMany("Commandes")
                        .HasForeignKey("LivreurId");
                });

            modelBuilder.Entity("GestionRestaurant.Models.ItemCommande", b =>
                {
                    b.HasOne("GestionRestaurant.Models.Commande")
                        .WithMany("Items")
                        .HasForeignKey("CommandeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GestionRestaurant.Models.ItemMenu", "Item")
                        .WithMany()
                        .HasForeignKey("ItemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GestionRestaurant.Models.ItemMenu", b =>
                {
                    b.HasOne("GestionRestaurant.Models.CatégorieMenu")
                        .WithMany("Items")
                        .HasForeignKey("Catégorie")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
